import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DatabaseMetaData;
import com.google.gson.Gson;
import java.util.Properties;
import kafka.admin.AdminUtils;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import java.util.Properties;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

// to compile - javac -cp "C:\softwareStore\kafka_2.12-2.2.0\libs\*;.\*;" tableToTopic.java
// to run - java -cp "C:\softwareStore\kafka_2.12-2.2.0\libs\*;.\*;" tableToTopic <table name>
public class tableToTopic {
    private static java.sql.Connection con = null;

    public static class kvp {
        String key;
        Object value;
    }

    public static void main(String[] args) {
        try {
            if(args.length < 1){
                System.out.println("Usage: tableToTopic <tableName>");
                return;
             }
            // Establish the Connection
            String url = "jdbc:datadirect:cassandra://localhost:9042;KeyspaceName=hr";
            con = DriverManager.getConnection(url, "admin", "adminpass");
            // Verify the Connection
            DatabaseMetaData metaData = con.getMetaData();
            System.out.println("Connected to " + metaData.getDatabaseProductName() + " " +  metaData.getDatabaseProductVersion());

            String tableName = args[0];                        
            
            //int retCode = processTable(tableName);
            System.out.printf("Creating topic (" + tableName + ")...");
            int retVal = createTopic(tableName); // tablename will be used as topic
            if (retVal == 0) {
                System.out.printf("OK\n");                
            }
            System.out.printf("Producing topic (" + tableName + ")...\n");
                List<kvp> list = null;
                retVal = processTable(tableName, list);
                if (retVal == 0) {
                    //retVal = produceTopic(tableName, list);
                    System.out.printf("End.\n");
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int createTopic(String topicName) throws SQLException {
        int retVal = -1;
        ZkClient zkClient = null;
        ZkUtils zkUtils = null;
        try {
            String zookeeperHosts = "localhost:2181"; // If multiple zookeeper then -> String zookeeperHosts = "192.168.20.1:2181,192.168.20.2:2181";
            int sessionTimeOutInMs = 15 * 1000; // 15 secs
            int connectionTimeOutInMs = 10 * 1000; // 10 secs

            zkClient = new ZkClient(zookeeperHosts, sessionTimeOutInMs, connectionTimeOutInMs, ZKStringSerializer$.MODULE$);
            zkUtils = new ZkUtils(zkClient, new ZkConnection(zookeeperHosts), false);

            int noOfPartitions = 1;
            int noOfReplication = 1;
            Properties topicConfiguration = new Properties();

            AdminUtils.createTopic(zkUtils, topicName, noOfPartitions, noOfReplication, topicConfiguration, null);
            retVal = 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (zkClient != null) {
                zkClient.close();
            }
        }
        return retVal;
    }

    

    public static int processTable(String tableName, List<kvp> list) throws SQLException {
        int retVal = -1;
        try {
            String t = "SELECT * FROM " + tableName + ";";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(t);
            
            Properties props = new Properties();      
            props.put("bootstrap.servers", "localhost:9092");      
            props.put("acks", "all");
            props.put("retries", 0);
            props.put("batch.size", 16384);
            props.put("linger.ms", 1);
            props.put("buffer.memory", 33554432);      
            props.put("key.serializer", Class.forName("org.apache.kafka.common.serialization.StringSerializer"));
            props.put("value.serializer", Class.forName("org.apache.kafka.common.serialization.StringSerializer"));
            Producer<String, String> producer = new KafkaProducer
                    <String, String>(props);

            if (list == null) {
                list = new ArrayList<kvp>();
            }
            int rowCount = 0;
            List<String> rowList = new ArrayList<String>();
            while (rs.next()) {
                for (int j = 1; j < rs.getMetaData().getColumnCount() + 1; j++) {
                    kvp _kvp = new kvp();
                    _kvp.key = rs.getMetaData().getColumnName(j);
                    _kvp.value = (Object)rs.getString(rs.getMetaData().getColumnName(j));
                    list.add(_kvp);
                    //System.out.println(_kvp.key + " : " + _kvp.value);
                }
                rowCount++;        
                String jsonString = "";
                Gson gson = new Gson();        
                jsonString = gson.toJson(list);
                list.clear();
                            
                producer.send(new ProducerRecord<String,String>(tableName, 
                Integer.toString(rowCount),jsonString));
               System.out.println("Producing " + jsonString + "...OK");
            }
            producer.close();
            retVal = 0;
        }   
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }   
        return retVal;
    }

}