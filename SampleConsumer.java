import java.util.Properties;
import java.util.Arrays;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DatabaseMetaData;
import com.google.gson.Gson;

public class SampleConsumer {
   private static java.sql.Connection con = null;

    public class emp {
        int empid;
        String emp_dept;
        String emp_first;
        String emp_last;
    }
   public static void main(String[] args) throws Exception {
      if(args.length < 1){
         System.out.println("Usage: consumer <topic> ");
         return;
      }
      
      String topic = args[0].toString();
      //String group = args[1].toString();
      Properties props = new Properties();
      props.put("bootstrap.servers", "localhost:9092");
      props.put("group.id", "SampleConsumer");
      props.put("enable.auto.commit", "true");
      props.put("auto.commit.interval.ms", "1000");
      props.put("session.timeout.ms", "30000");
      props.put("key.deserializer",          
         "org.apache.kafka.common.serialization.StringDeserializer");
      props.put("value.deserializer", 
         "org.apache.kafka.common.serialization.StringDeserializer");
      KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
      
      consumer.subscribe(Arrays.asList(topic));
      System.out.println("Subscribed to topic " + topic);
      int i = 0;
         
      while (true) {
         ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
               System.out.printf("offset = %d, key = %s, value = %s\n", 
               record.offset(), record.key(), record.value());
               //Process process = new ProcessBuilder("java -cp \".\\*;\" .\\sampleInsert ","emp",record.value()).start();

               //insert now
               try {
                  // Establish the Connection
                  String url = "jdbc:datadirect:cassandra://localhost:9042;KeyspaceName=hr";
                  con = DriverManager.getConnection(url, "admin", "adminpass");
                  // Verify the Connection
                  DatabaseMetaData metaData = con.getMetaData();
                  System.out.printf("Connected to Apache Cassandra %s", metaData.getDatabaseProductVersion());
                  //System.out.println("Database Name: " + metaData.getDatabaseProductName());
                  //System.out.println("Database Version: " + metaData.getDatabaseProductVersion());
      
                  //convert json string to java object
                  Gson gson = new Gson();
                  emp _emp = gson.fromJson(record.value(), emp.class);
                  
                  //System.out.printf("emp id = %d, first name = %s",_emp.empid, _emp.emp_first);
      
                  // insert now
                  String t = "insert into hr.emp values(" 
                  + _emp.empid + ", '"
                  + _emp.emp_dept + "', '"
                  + _emp.emp_first + "', '"
                  + _emp.emp_last + "');";
                  System.out.printf("\nInserting now the following record \n %s",t);
                  //System.out.printf("insert statement = %s",t);
                  PreparedStatement ps = con.prepareStatement(t);
                  ps.executeUpdate();
                  System.out.printf("\nDone");
              } catch (SQLException e) {
                  e.printStackTrace();
              }
            }
               
      }     
   }  
}